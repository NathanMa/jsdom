document.addEventListener('DOMContentLoaded', () => {

    let contentDiv = document.createElement("div")
    contentDiv.id = 'content'

    let menu = document.createElement("div")
    menu.className = "menu dark-grey"

    let target = document.createElement("div")
    target.className = "targets grey"

    let tab = [1, 2 ,1 ,2 ,2]
    for (let i = 1; i < 6; i++) {
        let cible = document.createElement("div")
        cible.className = "item light-grey"
        cible.innerHTML = "<h2>"+ i + "</h2><span>cible "+tab[i]+"</span>"
        menu.appendChild(cible)
    }
    
    let target1 = document.createElement("div")
    target1.className = "target pink"
    let h3target = document.createElement("h3")
    h3target.innerHTML = "cible 1"
    
    let spanTarget = document.createElement("span")
    spanTarget.className = "compteur"
    spanTarget.innerHTML = 0

    target1.appendChild(h3target)
    target1.appendChild(spanTarget)

    let target2 = document.createElement("div")
    target2.className = "target red"
    let h3target2 = document.createElement("h3")
    h3target2.innerHTML = "cible 2"
    
    let spanTarget2 = document.createElement("span")
    spanTarget2.className = "compteur"
    spanTarget2.innerHTML = 0
    target2.appendChild(h3target2)
    target2.appendChild(spanTarget2)
    
    target.appendChild(target1)
    target.appendChild(target2)
    contentDiv.appendChild(menu)
    contentDiv.appendChild(target)
    document.body.appendChild(contentDiv)
})
